{***********************************************************************
 * Unit Name: GX_eSampleEditorExpert
 * Purpose  : To show one way of coding an Editor Expert.
 *            This expert puts up a modal window and does nothing else
 * Author   : Scott Mattes <smattes@erols.com>
 ***********************************************************************}

unit GX_CreateMembersExpert;

{$I GX_CondDefine.inc}

interface

uses
  GX_Experts, GX_EditorExpert, GX_ConfigurationInfo, GX_OtaUtils,
  SysUtils, Classes, Controls, Forms, Dialogs, StdCtrls,ActnList,
  ExtCtrls, ComCtrls, ToolsAPI, Windows, GX_EditReader,strutils,GX_FileScanner;

type
  TGxCreateMembersExpert = class(TGX_Expert)
  private
    FData: string;
    module: IOTAModule;
    sourceFile: string;
    headerFile: string;

    function GetClassName(sourceFile: string): string;

  protected
  public
    function GetDisplayName: string; override;
    class function GetName: string; override;
    constructor Create; override;
    procedure Click(Sender: TObject); override;
//    procedure GetHelpString(List: TStrings); override;
    function HasConfigOptions: Boolean; override;
    function HasDesignerMenuItem: Boolean; override;
    function GetActionCaption: string; override;
    procedure UpdateAction(Action: TCustomAction); override;
  end;

type
  TfmCreateMembersExpertScreen = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    rgMemberType: TRadioGroup;
    edIdentifier: TLabeledEdit;
    cbType: TComboBox;
    laType: TLabel;
    cbSetMethod: TCheckBox;
    cbGetMethod: TCheckBox;
    cbCreateField: TCheckBox;
    edSetMethod: TEdit;
    edGetMethod: TEdit;
    edField: TEdit;
    cbImplementSetter: TCheckBox;
    cbImplementGetter: TCheckBox;
    edClassName: TLabeledEdit;
    edParameters: TLabeledEdit;
    cbFastCall: TCheckBox;
    procedure btnOKClick(Sender: TObject);
    procedure cbSetMethodClick(Sender: TObject);
    procedure cbGetMethodClick(Sender: TObject);
    procedure cbCreateFieldClick(Sender: TObject);
    procedure edIdentifierChange(Sender: TObject);
    procedure rgMemberTypeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  public
    headerText: string;
    sourceText: string;
    settersText: string;
    fieldText: string;
  end;

implementation

{$R *.dfm}

uses
  {$IFOPT D+} GX_DbugIntf, {$ENDIF}
  Menus, Registry,
  GX_GenericUtils,
  GX_GExperts,GX_DbugIntf;

{ TGxSampleEditorExpert }

//*********************************************************
//    Name: TGxSampleEditorExpert.Create
// Purpose: Sets up basic information about your editor expert
//   Notes: The value of ShortCut is touchy, use the ShortCut
//          button on the Editor Experts tab of menu item
//          GExperts/GExperts Configuration... on an existing
//          editor expert to see if you can use a specific
//          combination for your expert.
//*********************************************************
constructor TGxCreateMembersExpert.Create;
begin
  inherited Create;

  // The default shortcut key to activate your editor expert.
//  ShortCut := scCtrl + scShift + Ord('M');
  ShortCut := scAlt + Ord('M');

  // Set default values for any data
  FData := 'editor data';
end;

function TGxCreateMembersExpert.GetClassName(sourceFile: string): string;
var
  classPos: Integer;
  endClassNamePos: Integer;
begin
  if not FindTextIdent('PACKAGE', sourceFile, 0, False, classPos) then
    FindTextIdent('class', sourceFile, 0, False, classPos);
  while ((sourceFile[classPos] in ['a'..'z','A'..'Z']))
            or (classPos>=Length(sourceFile))
    do Inc(classPos);
  endClassNamePos:=classPos+1;
  while ((sourceFile[endClassNamePos] in ['a'..'z','A'..'Z']))
            or (endClassNamePos>=Length(sourceFile))
    do Inc(endClassNamePos);
  Result:=Trim(MidStr(sourceFile,classPos,endClassNamePos-classPos));
end;

//*********************************************************
//    Name: TGxSampleEditorExpert.Execute
// Purpose: Called when your hot-key is pressed, this is
//          where you code what the expert should do
//*********************************************************
procedure TGxCreateMembersExpert.Click(Sender: TObject);
resourcestring
  SNotice = 'This is where your editor expert would do something useful.';
var
  headerAsString: string;
  privatePos: Integer;
  crlfPos: Integer;
  protectedPos: Integer;
  lastInPrivatePos: Integer;
  insertPos: Integer;
  classIdent: string;
begin
  module:=GxOtaGetCurrentModule;
  if module.ModuleFileCount<2 then Exit;

  sourceFile:=module.ModuleFileEditors[0].FileName;
  headerFile:=module.ModuleFileEditors[1].FileName;

  if headerFile<>GxOtaGetCurrentSourceFile then
  begin
    MessageDlg('Use on header file ', mtError, [mbOK], 0);
    Exit;
  end;
  with TfmCreateMembersExpertScreen.Create(nil) do
  try
    GxOtaGetActiveEditorTextAsString(headerAsString,False);
    classIdent:=GetClassName(headerAsString);
    if classIdent='' then
    begin
      MessageDlg('No class found. Declare a class in the header file.', mtError, [mbOK], 0);
      Exit;
    end;
    edClassName.Text:=classIdent;
    if ShowModal = mrOk then
    begin
      if (not FindTextIdent('private', headerAsString, GxOtaGetCurrentEditBufferPos, True, privatePos)) then
      begin
        if (FindTextIdent('class', headerAsString, GxOtaGetCurrentEditBufferPos, True, privatePos)) then
        begin
          FindTextIdent('{', headerAsString, privatePos, False, privatePos);
          GxOtaInsertTextIntoEditorAtPos(sLineBreak+'private:'+fieldText,privatePos+1);
//          privatePos=privatePos+8;
        end
        else
        begin
          MessageDlg('Class not found. Use header file.', mtError, [mbOK], 0);
          Exit;
        end;
      end;
      insertPos:=privatePos+7;
      if (FindTextIdent(sLineBreak+sLineBreak,headerAsString,privatePos,False,crlfPos)) then
        insertPos:=crlfPos;
      if (FindTextIdent('protected',headerAsString,privatePos,False,protectedPos)) then
      begin
        if (protectedPos<crlfPos) then insertPos:=privatePos+7;
      end
      else
      begin
        if (FindTextIdent('public',headerAsString,privatePos,False,protectedPos)) then
        begin
          if (protectedPos<crlfPos) then insertPos:=privatePos+7;
        end
        else protectedPos:=privatePos;
      end;
      if (not FindTextIdent(';',headerAsString,protectedPos,True,lastInPrivatePos))then
        lastInPrivatePos:=insertPos;
      GxOtaInsertTextIntoEditor(settersText);
      GxOtaInsertTextIntoEditorAtPos(fieldText,insertPos);
      GxOtaInsertTextIntoEditor(headerText+sourceText);
//      GxOtaInsertTextIntoEditorAtPos(sourceText,Position: Longint;  SourceEditor: IOTASourceEditor = nil);
//      GxOtaInsertTextIntoEditor(' '+GetClassName);
    end;
  finally
    Free;
  end;
end;

//*********************************************************
//    Name: TGxSampleEditorExpert.DisplayName
// Purpose: The expert name that appears in Editor Experts box on the
//          Editor tab of menu item GExperts/GExperts Configuration...
//          Experts tab on menu item GExperts/GExperts Configuration...
//*********************************************************
function TGxCreateMembersExpert.GetDisplayName: string;
resourcestring
  SDisplayName = 'Create Members Expert';
begin
  Result := SDisplayName;
end;
{
//*********************************************************
//    Name: TGxSampleEditorExpert.GetHelpString
// Purpose: To provide your text on what this editor expert
//          does to the expert description box on the Editor
//          Experts tab on menu item GExperts/GExperts Configuration...
//*********************************************************
procedure TGxCreateMembersExpert.GetHelpString(List: TStrings);
resourcestring
  SSampleEditorExpertHelp =
    'Create class members with a dialog box ';
begin
  List.Text := SSampleEditorExpertHelp;
end;
}
//*********************************************************
//    Name: TGxSampleEditorExpert.GetName
// Purpose: Each editor expert needs to provide a name
//          that represents this editor expert.
//          An empty file name means that there is no
//          iconic representation.
//*********************************************************
class function TGxCreateMembersExpert.GetName: string;
const
  SName = 'CreateMembersEditorExpert';
begin
  Result := SName;
end;

//*********************************************************
//    Name: TGxSampleEditorExpert.HasConfigOptions
// Purpose: Let the world know whether this expert has
//          configuration options.
//*********************************************************
function TGxCreateMembersExpert.HasConfigOptions: Boolean;
begin
  Result := False;
end;

function TGxCreateMembersExpert.HasDesignerMenuItem: Boolean;
begin
  Result:= False;
end;

procedure TGxCreateMembersExpert.UpdateAction(Action: TCustomAction);
begin
  inherited;
  Action.Enabled:=GxOtaCurrentProjectIsNativeCpp;
end;

function TGxCreateMembersExpert.GetActionCaption: string;
resourcestring
  SMenuCaption = 'Create Members...';
begin
  Result := SMenuCaption;
end;

//*******************************************************************
// Purpose: Tells GExperts about the existance of this editor expert
//*******************************************************************
procedure TfmCreateMembersExpertScreen.btnOKClick(Sender: TObject);
const
  tab=char(9);
  eol=char(10)+char(13);
var
  modifier : AnsiString;
begin
  if cbFastCall.Checked then
    modifier := '__fastcall '
  else
    modifier := '';

  case rgMemberType.ItemIndex of
    0:begin    //constructor
      edIdentifier.Text:=edClassName.Text;
      headerText:=Tab+modifier+edClassName.Text+'('+edParameters.Text+');'+eol;
      sourceText:=eol+modifier+edClassName.Text+'::'+edClassName.Text+'( '+edParameters.Text+' ) {'+eol+Tab+'//TODO: Implement constructor'+eol+'}'+eol+eol;
    end;
    1: begin   //destructor
      edIdentifier.Text:='~'+edClassName.Text;
      headerText:=Tab+modifier+edIdentifier.Text+'();'+eol;
      sourceText:=eol+modifier+edClassName.Text+'::'+edIdentifier.Text+'() {'+eol+Tab+'//TODO: Implement destructor'+eol+'}'+eol+eol;
    end;
    2: begin   //method
      headerText:=tab+cbType.Text+' ' + modifier+edIdentifier.Text+'( '+edParameters.Text+' );'+eol;
      sourceText:=eol+cbType.Text+' ' + modifier+edClassName.Text+'::'+edIdentifier.Text+'( '+edParameters.Text+' ) {'+eol+Tab+'//TODO: Implement method'+eol+'}'+eol+eol;
    end;
    3: begin  //property
        sourceText:='';
        headerText:=tab+'.property( "'+edIdentifier.Text+'", ';
        if (cbSetMethod.Checked) then
        begin
          headerText:=headerText+'&'+edClassName.Text+'::'+edSetMethod.Text;
          if (cbGetMethod.Checked) then headerText:=headerText+', ';
        end;
        if (cbGetMethod.Checked) then headerText:=headerText+'&'+edClassName.Text+'::'+edGetMethod.Text;
        headerText:=headerText+' )'+eol;
        if cbCreateField.Checked then fieldText:=eol+tab+cbType.Text+' '+edField.Text+';';
        if cbSetMethod.Checked then
        begin
          settersText:=eol+tab+'void '+ modifier+edSetMethod.Text+'('+cbType.Text+' value);';
          sourceText:=eol+'void '+modifier+edClassName.Text+'::'+edSetMethod.Text+'('+cbType.Text+' value) {'+eol+Tab;
          if cbImplementSetter.Checked then sourceText:=sourceText+'if ( '+edField.Text+' != value ) {'+eol+tab+Tab+edField.Text+' = value;'+eol+tab+Tab+'onChange().fire( this, PropertyChangedEvent( "'+edIdentifier.Text+'" ) );'+eol+Tab+'}'+eol+'}'
          else sourceText:=sourceText+'//TODO: Implement setter'+eol+'}';
          sourceText:=sourceText+eol;
        end;
        if cbGetMethod.Checked then
        begin
          settersText:=settersText+eol+tab+cbType.Text+' ' + modifier+edGetMethod.Text+'();'+eol;
          sourceText:=sourceText+eol+cbType.Text+' ' + modifier+edClassName.Text+'::'+edGetMethod.Text+'() {'+eol+Tab;
          if cbImplementGetter.Checked then sourceText:=sourceText+'return '+edField.Text+';'+eol+'}'
          else sourceText:=sourceText+'//TODO: Implement getter'+eol+'}';
          sourceText:=sourceText+eol;
        end;
//        if sourceText<>'' then sourceText:=sourceText+eol;
    end;
        4: begin  // VCL property
        sourceText:='';
        headerText:=tab+'__property '+cbType.Text+' '+edIdentifier.Text+' = { read=';
        if (cbGetMethod.Checked) then headerText:=headerText+edGetMethod.Text+', write='
        else headerText:=headerText+edField.Text+', write=';
        if (cbSetMethod.Checked) then headerText:=headerText+edSetMethod.Text+' };'+eol
        else headerText:=headerText+edField.Text+' };'+eol;
        if cbCreateField.Checked then fieldText:=eol+tab+cbType.Text+' '+edField.Text+';';
        if cbSetMethod.Checked then
        begin
          settersText:=eol+tab+'void '+' ' + modifier+edSetMethod.Text+'( '+cbType.Text+' value );';
          sourceText:=eol+'void __fastcall '+edClassName.Text+'::'+edSetMethod.Text+'('+cbType.Text+' value)'+eol+'{'+eol+Tab;
          if cbImplementSetter.Checked then sourceText:=sourceText+'if ('+edField.Text+'!=value)'+eol+Tab+'{'+eol+tab+Tab+edField.Text+'=value;'+eol+Tab+'}'+eol+'}'
          else sourceText:=sourceText+'//TODO: Implement setter'+eol+'}';
          sourceText:=sourceText+eol;
        end;
        if cbGetMethod.Checked then
        begin
          settersText:=settersText+eol+tab+cbType.Text+' ' + modifier+edGetMethod.Text+'();'+eol;
          sourceText:=sourceText+eol+cbType.Text+' ' + modifier+edClassName.Text+'::'+edGetMethod.Text+'()'+eol+'{'+eol+Tab;
          if cbImplementGetter.Checked then sourceText:=sourceText+'return '+edField.Text+';'+eol+'}'
          else sourceText:=sourceText+'//TODO: Implement getter'+eol+'}';
          sourceText:=sourceText+eol;
        end;
//        if sourceText<>'' then sourceText:=sourceText+eol;
    end;
  end;
end;

procedure TfmCreateMembersExpertScreen.cbCreateFieldClick(
  Sender: TObject);
var
  lower: string;
  c: Char;
begin
  if edIdentifier.Text<>'' then
  begin
    lower:=edIdentifier.Text;
    c:=AnsiLowerChar(lower[1]);
    lower[1]:=c;
    edField.Text:='_'+lower;
  end;
end;

procedure TfmCreateMembersExpertScreen.cbGetMethodClick(Sender: TObject);
begin
  edGetMethod.Text:=edIdentifier.Text;
end;

procedure TfmCreateMembersExpertScreen.cbSetMethodClick(Sender: TObject);
var
  upper: string;
  c: Char;
begin
  upper:=edIdentifier.Text;
  c:=AnsiUpperChar(upper[1]);
  upper[1]:=c;
  edSetMethod.Text:='set'+edIdentifier.Text;
end;

procedure TfmCreateMembersExpertScreen.edIdentifierChange(
  Sender: TObject);
var
  upper: string;
  c: Char;
begin
  upper:=edIdentifier.Text;
  c:=AnsiUpperChar(upper[1]);
  upper[1]:=c;
  edSetMethod.Text:='set'+upper;
  edGetMethod.Text:=edIdentifier.Text;
  cbCreateFieldClick(nil);
end;

procedure TfmCreateMembersExpertScreen.FormShow(Sender: TObject);
begin
  edIdentifier.SetFocus;
end;

procedure TfmCreateMembersExpertScreen.rgMemberTypeClick(Sender: TObject);
begin
  case rgMemberType.ItemIndex of
    0:begin
      edIdentifier.Enabled:=False;
      edParameters.Enabled:=True;
      cbType.Enabled:=False;
      laType.Enabled:=False;
      cbSetMethod.Enabled:=False;
      edSetMethod.Enabled:=False;
      cbImplementSetter.Enabled:=False;
      cbGetMethod.Enabled:=False;
      edGetMethod.Enabled:=False;
      cbImplementGetter.Enabled:=False;
      cbCreateField.Enabled:=False;
      edField.Enabled:=False;
      edParameters.SetFocus;
    end;
    1:begin
      edIdentifier.Enabled:=False;
      edParameters.Enabled:=False;
      cbType.Enabled:=False;
      laType.Enabled:=False;
      cbSetMethod.Enabled:=False;
      edSetMethod.Enabled:=False;
      cbImplementSetter.Enabled:=False;
      cbGetMethod.Enabled:=False;
      edGetMethod.Enabled:=False;
      cbImplementGetter.Enabled:=False;
      cbCreateField.Enabled:=False;
      edField.Enabled:=False;
    end;
    2:begin
      edIdentifier.Enabled:=True;
      edParameters.Enabled:=True;
      cbType.Enabled:=True;
      laType.Enabled:=True;
      cbSetMethod.Enabled:=False;
      edSetMethod.Enabled:=False;
      cbImplementSetter.Enabled:=False;
      cbGetMethod.Enabled:=False;
      edGetMethod.Enabled:=False;
      cbImplementGetter.Enabled:=False;
      cbCreateField.Enabled:=False;
      edField.Enabled:=False;
      edIdentifier.SetFocus;
    end;
    3:begin
      edIdentifier.Enabled:=True;
      edParameters.Enabled:=False;
      cbType.Enabled:=True;
      laType.Enabled:=True;
      cbSetMethod.Enabled:=True;
      edSetMethod.Enabled:=True;
      cbImplementSetter.Enabled:=True;
      cbGetMethod.Enabled:=True;
      edGetMethod.Enabled:=True;
      cbImplementGetter.Enabled:=True;
      cbCreateField.Enabled:=True;
      edField.Enabled:=True;
      edIdentifier.SetFocus;
      cbImplementSetter.Checked := True;
      cbImplementGetter.Checked := True;
    end;
  end;
end;

initialization
  RegisterGX_Expert(TGxCreateMembersExpert);
end.

