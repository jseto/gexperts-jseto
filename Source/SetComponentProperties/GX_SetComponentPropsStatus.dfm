object fmSetComponentPropsStatus: TfmSetComponentPropsStatus
  Left = 433
  Top = 371
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Set Component Properties Status'
  ClientHeight = 65
  ClientWidth = 497
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object grpbxStatus: TGroupBox
    Left = 8
    Top = 8
    Width = 481
    Height = 49
    Caption = 'Processing File'
    TabOrder = 0
    object pnlProcessedFile: TPanel
      Left = 8
      Top = 16
      Width = 465
      Height = 25
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
end
