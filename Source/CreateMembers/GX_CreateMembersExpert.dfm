object fmCreateMembersExpertScreen: TfmCreateMembersExpertScreen
  Left = 381
  Top = 212
  BorderStyle = bsDialog
  Caption = 'Create Member Editor'
  ClientHeight = 400
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object laType: TLabel
    Left = 12
    Top = 166
    Width = 24
    Height = 13
    Caption = 'Type'
  end
  object btnOK: TButton
    Left = 80
    Top = 367
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 13
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 185
    Top = 367
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 14
  end
  object rgMemberType: TRadioGroup
    Left = 11
    Top = 38
    Width = 323
    Height = 53
    Caption = 'Member Type'
    Columns = 4
    ItemIndex = 2
    Items.Strings = (
      'Constructor'
      'Destructor'
      'Method'
      'Property')
    TabOrder = 1
    OnClick = rgMemberTypeClick
  end
  object edIdentifier: TLabeledEdit
    Left = 46
    Top = 101
    Width = 286
    Height = 21
    EditLabel.Width = 28
    EditLabel.Height = 13
    EditLabel.Caption = 'Name'
    LabelPosition = lpLeft
    LabelSpacing = 5
    TabOrder = 2
    OnChange = edIdentifierChange
  end
  object cbType: TComboBox
    Left = 45
    Top = 164
    Width = 145
    Height = 21
    ItemHeight = 0
    TabOrder = 4
    Text = 'void'
    Items.Strings = (
      'char'
      'unsigned char'
      'int'
      'unsigned int'
      'float'
      'double'
      'AnsiString'
      'void')
  end
  object cbSetMethod: TCheckBox
    Left = 15
    Top = 197
    Width = 115
    Height = 17
    Caption = 'Create Set Method'
    Checked = True
    Enabled = False
    State = cbChecked
    TabOrder = 5
    OnClick = cbSetMethodClick
  end
  object cbGetMethod: TCheckBox
    Left = 14
    Top = 255
    Width = 115
    Height = 17
    Caption = 'Create Get Method'
    Checked = True
    Enabled = False
    State = cbChecked
    TabOrder = 8
    OnClick = cbGetMethodClick
  end
  object cbCreateField: TCheckBox
    Left = 14
    Top = 312
    Width = 97
    Height = 17
    Caption = 'Create Field'
    Checked = True
    Enabled = False
    State = cbChecked
    TabOrder = 11
    OnClick = cbCreateFieldClick
  end
  object edSetMethod: TEdit
    Left = 140
    Top = 195
    Width = 190
    Height = 21
    Enabled = False
    TabOrder = 6
  end
  object edGetMethod: TEdit
    Left = 139
    Top = 252
    Width = 190
    Height = 21
    Enabled = False
    TabOrder = 9
  end
  object edField: TEdit
    Left = 139
    Top = 309
    Width = 190
    Height = 21
    Enabled = False
    TabOrder = 12
  end
  object cbImplementSetter: TCheckBox
    Left = 141
    Top = 220
    Width = 149
    Height = 17
    Caption = 'Implement Setter with field'
    Checked = True
    Enabled = False
    State = cbChecked
    TabOrder = 7
  end
  object cbImplementGetter: TCheckBox
    Left = 140
    Top = 277
    Width = 146
    Height = 17
    Caption = 'Implement Getter with field'
    Checked = True
    Enabled = False
    State = cbChecked
    TabOrder = 10
  end
  object edClassName: TLabeledEdit
    Left = 72
    Top = 10
    Width = 260
    Height = 21
    EditLabel.Width = 56
    EditLabel.Height = 13
    EditLabel.Caption = 'Class Name'
    LabelPosition = lpLeft
    LabelSpacing = 5
    TabOrder = 0
  end
  object edParameters: TLabeledEdit
    Left = 76
    Top = 131
    Width = 255
    Height = 21
    EditLabel.Width = 59
    EditLabel.Height = 13
    EditLabel.Caption = 'Paramenters'
    LabelPosition = lpLeft
    LabelSpacing = 5
    TabOrder = 3
  end
  object cbFastCall: TCheckBox
    Left = 15
    Top = 344
    Width = 97
    Height = 17
    Caption = 'Fastcall'
    TabOrder = 15
  end
end
