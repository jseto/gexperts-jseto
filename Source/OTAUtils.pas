unit OTAUtils;

interface

uses ToolsAPI, DesignIntf;

function GetWizardService: IOTAWizardServices;
function GetModuleServices: IOTAModuleServices;
//function GetProjectManager: IOTAProjectManager;
//function GetHistoryServices: IOTAHistoryServices;
//function GetStructureView: IOTAStructureView;
function GetEditorServices: IOTAEditorServices;
function GetCurrentModule: IOTAModule;
function GetCurrentEditView: IOTAEditView;
function GetCurrentSourceEditor: IOTASourceEditor;
function GetServices: IOTAServices;
function GetActionServices: IOTAActionServices;
procedure LogMessage(const Msg: string);
function GetMessageServices: IOTAMessageServices;
function GetCurrentProject: IOTAProject;
function GetCurrentProjectGroup: IOTAProjectGroup;
function GetFormEditorFromModule(const Module: IOTAModule): IOTAFormEditor;
function GetCurrentFormEditor: IOTAFormEditor;
//function GetCurrentFormDesigner: IDesigner80;
function GetSelectedComponent: IOTAComponent;
function GetPropertyIndex(Component: IOTAComponent; const Name: WideString): Integer;
function GetEnvironmentOptions: IOTAEnvironmentOptions;
function GetEnvironmentOption(const OptionName: string): string;

// Generic utilities
function LastCharPos(const S: string; C: Char): Integer;
function LastDirectory(const Path: string): string;

type
  TMenuItemWizard = class(TNotifierObject, IOTAMenuWizard, IOTAWizard)
    FName: string;
    FAuthor: string;
    FIDString: string;
    FMenuText: string;
  public
    procedure Execute; virtual; abstract;
    function GetIDString: string;
    function GetMenuText: string;
    function GetName: string;
    function GetState: TWizardState;
    constructor Create(const Name: string; MenuText: string = 'Execute Wizard';
      Author: string = 'Author'; IDString: string = '');
  end;

implementation

uses SysUtils, Classes, Dialogs;

function GetWizardService: IOTAWizardServices;
begin
  Result := BorlandIDEServices as IOTAWizardServices;
end;

function GetModuleServices: IOTAModuleServices;
begin
  Result := BorlandIDEServices as IOTAModuleServices;
  if not Assigned(Result) then
    raise Exception.Create('No module services interface');
end;

(*
function GetProjectManager: IOTAProjectManager;
begin
  Result := BorlandIDEServices as IOTAProjectManager;
  if not Assigned(Result) then
    raise Exception.Create('No project manager interface');
end;

function GetHistoryServices: IOTAHistoryServices;
begin
  Result := BorlandIDEServices as IOTAHistoryServices;
  if not Assigned(Result) then
    raise Exception.Create('No history services interface');
end;

function GetStructureView: IOTAStructureView;
begin
  Result := BorlandIDEServices as IOTAStructureView;
  if not Assigned(Result) then
    raise Exception.Create('No structure view interface');
end;

function GetCurrentFormDesigner: IDesigner80;
var
  FormEditor: IOTAFormEditor;
begin
  FormEditor := GetCurrentFormEditor;
  if Assigned(FormEditor) then begin
    Result := (FormEditor as INTAFormEditor).FormDesigner;
    if not Assigned(Result) then
      raise Exception.Create('No curent designer');
  end
  else
    raise Exception.Create('No current form editor');
end;
*)

function GetEditorServices: IOTAEditorServices;
begin
  Result := BorlandIDEServices as IOTAEditorServices;
  if not Assigned(Result) then
    raise Exception.Create('No editor services interface');
end;

function GetServices: IOTAServices;
begin
  Result := BorlandIDEServices as IOTAServices;
  if not Assigned(Result) then
    raise Exception.Create('No services interface');
end;

function GetActionServices: IOTAActionServices;
begin
  Result := BorlandIDEServices as IOTAActionServices;
  if not Assigned(Result) then
    raise Exception.Create('No action services interface');
end;

function GetMessageServices: IOTAMessageServices;
begin
  Result := BorlandIDEServices as IOTAMessageServices;
  if not Assigned(Result) then
    raise Exception.Create('No message services interface');
end;

procedure LogMessage(const Msg: string);
begin
  GetMessageServices.AddTitleMessage(Msg);
end;

function GetCurrentModule: IOTAModule;
begin
  Result := GetModuleServices.CurrentModule;
end;

function GetCurrentEditView: IOTAEditView;
var
  SourceEditor: IOTASourceEditor;
begin
  Result := nil;
  SourceEditor := GetCurrentSourceEditor;
  if Assigned(SourceEditor) and (SourceEditor.EditViewCount > 0) then
    Result := SourceEditor.EditViews[0];
end;

function GetCurrentSourceEditor: IOTASourceEditor;
var
  Module: IOTAModule;
  i: Integer;
  Editor: IOTAEditor;
begin
  Result := nil;
  Module := GetCurrentModule;
  if not Assigned(Module) then
    Exit;
  for i := 0 to Module.ModuleFileCount - 1 do
  begin
    Editor := Module.ModuleFileEditors[i];
    if Supports(Editor, IOTASourceEditor) then
    begin
      Result := Editor as IOTASourceEditor;
      Break;
    end;
  end;
end;

function GetCurrentProjectGroup: IOTAProjectGroup;
var
  IModuleServices: IOTAModuleServices;
  IModule: IOTAModule;
  i: Integer;
begin
  Assert(Assigned(BorlandIDEServices));

  IModuleServices := BorlandIDEServices as IOTAModuleServices;
  Assert(Assigned(IModuleServices));

  Result := nil;
  for i := 0 to IModuleServices.ModuleCount - 1 do
  begin
    IModule := IModuleServices.Modules[i];
    if Supports(IModule, IOTAProjectGroup, Result) then
      Break;
  end;
end;

function GetCurrentProject: IOTAProject;
var
  IProjectGroup: IOTAProjectGroup;
  IModuleServices: IOTAModuleServices;
  IModule: IOTAModule;
  i: Integer;
begin
  Result := nil;

  IProjectGroup := GetCurrentProjectGroup;
  if not Assigned(IProjectGroup) then
  begin
    Assert(Assigned(BorlandIDEServices));
    IModuleServices := BorlandIDEServices as IOTAModuleServices;
    Assert(Assigned(IModuleServices));

    for i := 0 to IModuleServices.ModuleCount - 1 do
    begin
      IModule := IModuleServices.Modules[i];
      if Supports(IModule, IOTAProject, Result) then
        Break;
    end;
  end;

  try
    // This raises exceptions in D5 with .bat projects active
    if Assigned(IProjectGroup) and (not Assigned(Result)) then
      Result := IProjectGroup.ActiveProject;
  except
    Result := nil;
  end;
end;

function GetFormEditorFromModule(const Module: IOTAModule): IOTAFormEditor;
var
  i: Integer;
  Editor: IOTAEditor;
  FormEditor: IOTAFormEditor;
begin
  Result := nil;
  if not Assigned(Module) then
    Exit;
  for i := 0 to Module.GetModuleFileCount-1 do
  begin
    Editor := Module.GetModuleFileEditor(i);
    if Supports(Editor, IOTAFormEditor, FormEditor) then
    begin
      Result := FormEditor;
      Break;
    end;
  end;
end;

function GetCurrentFormEditor: IOTAFormEditor;
begin
  Result := GetFormEditorFromModule(GetCurrentModule);
end;

function GetSelectedComponent: IOTAComponent;
var
  FormEditor: IOTAFormEditor;
begin
  Result := nil;
  FormEditor := GetCurrentFormEditor;
  if not Assigned(FormEditor) then
    Exit;
  if FormEditor.GetSelCount < 1 then
    Exit;
  Result := FormEditor.GetSelComponent(0);
end;

function GetPropertyIndex(Component: IOTAComponent; const Name: WideString): Integer;
var
  i: Integer;
  PropName: string;
begin
  Result := -1;
  Assert(Assigned(Component));

  for i := 0 to Component.GetPropCount - 1 do begin
    PropName := Component.GetPropName(i);
    if SameText(Name, PropName) then
    begin
      Result := i;
      Break;
    end;
  end;
end;

function GetEnvironmentOptions: IOTAEnvironmentOptions;
var
  Services: IOTAServices;
begin
  Services := BorlandIDEServices as IOTAServices;
  Assert(Assigned(Services), 'IOTAServices not available');
  Result := Services.GetEnvironmentOptions;
end;

function GetEnvironmentOption(const OptionName: string): string;
var
  EnvironmentOptions: IOTAEnvironmentOptions;
begin
  EnvironmentOptions := GetEnvironmentOptions;
  Assert(Assigned(EnvironmentOptions), 'IOTAEnvironmentOptions not available');
  Result := EnvironmentOptions.GetOptionValue(OptionName);
end;

function LastCharPos(const S: string; C: Char): Integer;
var
  i: Integer;
begin
  i := Length(S);
  while (i > 0) and (S[i] <> C) do
    Dec(i);
  Result := i;
end;

function LastDirectory(const Path: string): string;
var
  LastDirPos: Integer;
begin
  Result := ExcludeTrailingPathDelimiter(Path);
  LastDirPos := LastCharPos(Result, PathDelim);
  Result := Copy(Result, LastDirPos + 1, MaxInt);
end;

{ TMenuItemAddin }

constructor TMenuItemWizard.Create(const Name: string; MenuText: string = 'Execute Wizard';
      Author: string = 'Author'; IDString: string = '');
begin
  FName := Name;
  FMenuText := MenuText;
  FAuthor := Author;
  if Trim(IDString) = '' then
    FIDString := StringReplace(FAuthor + FName + IntToStr(Random(10000)), ' ', '.', [rfReplaceAll])
  else
    FIDString := IDString;
end;

function TMenuItemWizard.GetIDString: string;
begin
  Result := FIDString;
end;

function TMenuItemWizard.GetMenuText: string;
begin
  Result := FMenuText;
end;

function TMenuItemWizard.GetName: string;
begin
  Result := FName;
end;

function TMenuItemWizard.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

end.

